function load_directions(){

    const id_customer = $('#id_customer').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/clientes/direcciones/get/all/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_customer': id_customer,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#body_directions').html("")

        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].country_name + '</td>'
            row += '<td>' + data[i].city_name + '</td>'
            row += '<td>' + data[i].address + ', ' + data[i].zip_code + ', (' + data[i].reference + ')</td>'
            row += '<td>' + data[i].phone + '</td>'
            row += '<td><button class="btn btn-info btn-sm direction_edit" title="Editar dirección" id="direction_edit_'+ data[i].id +'"><i class="fa fa-edit"></i></button><button class="btn btn-danger btn-sm direction_delete" title="Eliminar dirección" id="direction_delete_'+ data[i].id +'"><i class="fa fa-trash"></i></button></td>'
            row += '</tr>'
            $('#body_directions').append(row)
        }  
        

    }).fail(function(data){
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })

}