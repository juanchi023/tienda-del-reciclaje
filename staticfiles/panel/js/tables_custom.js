function list_versions(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/version/get/ajax/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable').DataTable().destroy()
        $('#kt_datatable tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].created_by_at + '</td>'
            row += '<td>' + data[i].title + '</td>'
            row += '<td>' + data[i].description + '</td>'
            row += '<td><a href="/panel/customers/projects/' + data[i].project_slug + '/details/versions/' + data[i].id + '/edit/" title="Editar"><button class="btn btn-success btn_detail_activity btn-icon"><i class="fa fa-edit"></i></button></a>'
            
            if(data[i].has_planned_curve == true){
                row += '<button class="btn btn-primary btn-icon btn_edit_planned_curve" id="edit_curve_progress_' + data[i].planned_curve_id + '" title="Editar progreso planeado" value="' + data[i].title + '"><i class="fa fa-edit"></i></button>'
            }else{
                row += '<button class="btn btn-warning btn-icon open_modal_curve" id="add_curve_progress_' + data[i].id + '" title="Crear progreso planeado" value="' + data[i].title + '"><i class="fa fa-plus"></i></button>'
            }
            
            row += '<button class="btn btn-danger btn_delete_version btn-icon" id="delete_version_' + data[i].id + '" title="Eliminar"><i class="fa fa-trash"></i></button></td>'

            row += '</tr>'
            $('#kt_datatable').append(row)            
        }   
        $('#kt_datatable').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_activities(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/get/ajax/activity/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable').DataTable().destroy()
        $('#kt_datatable tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].version + '</td>'
            row += '<td>' + data[i].code + '</td>'
            row += '<td>' + data[i].qqty_estimated + '</td>'
            row += '<td>' + data[i].executed_work + ' (' +  data[i].executed_work_percent + '%)</td>'
            row += '<td>' + data[i].item_budget + '</td>'
            row += '<td><button class="btn btn-primary btn-icon btn_details_activity" id="details_activity_' + data[i].id + '" title="Ver detalles"><i class="fa fa-search"></i></button><a href="/panel/customers/projects/' + data[i].project_slug + '/details/activities/' + data[i].id + '/edit/" title="Editar"><button class="btn btn-success btn-icon"><i class="fa fa-edit"></i></button></a><button class="btn btn-warning btn-icon btn_create_progress" id="add_activity_progress_' + data[i].id + '" title="Añadir progreso"><i class="fa fa-plus"></i></button><button class="btn btn-danger btn_delete_activity btn-icon" id="delete_activity_' + data[i].id + '" title="Eliminar"><i class="fa fa-trash"></i></button></td>'
            row += '</tr>'
            $('#kt_datatable').append(row)            
        }   
        $('#kt_datatable').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_all_activity_progress(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/all/activity-progress/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable_2').DataTable().destroy()
        $('#kt_datatable_2 tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].activity + '</td>'
            row += '<td>' + data[i].progress_date + '</td>'
            row += '<td>' + data[i].progress_quantity_work + '</td>'
            row += '<td>' + data[i].progress_man_hours + '</td>'
            row += '</tr>'
            $('#kt_datatable_2').append(row)            
        }   
        $('#kt_datatable_2').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_activity_progress(id_activity){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/filter/activity-progress/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'id_activity': id_activity,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable_1_2').DataTable().destroy()
        $('#kt_datatable_1_2 tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].progress_date + '</td>'
            row += '<td>' + data[i].progress_quantity_work + '</td>'
            row += '<td>' + data[i].progress_man_hours + '</td>'
            row += '<td><button class="btn btn-success get_activity_progress btn-icon" id="edit_activity_progress_' + data[i].id + '" title="Editar"><i class="fa fa-edit"></i></button><button class="btn btn-danger btn_delete_activity_progress btn-icon" id="delete_activity_progress_' + data[i].id + '" title="Eliminar"><i class="fa fa-trash"></i></button></td>'
            row += '</tr>'
            $('#kt_datatable_1_2').append(row)            
        }   
        $('#kt_datatable_1_2').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_budgets(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/list/ajax/budget/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){

        $('#kt_datatable').DataTable().destroy()
        $('#kt_datatable tbody').html("")

        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].title + '</td>'
            row += '<td>' + data[i].total_qtty + '</td>'
            row += '<td>$ ' + data[i].budget + '</td>'
            row += '<td>$ ' + data[i].spent_budget + '</td>'
            row += '<td>$ ' + data[i].remaining_budget + '</td>'
            row += '<td><button class="btn btn-primary btn-icon btn_details_budget" id="details_budget_' + data[i].id + '" title="Ver detalles"><i class="fa fa-search"></i></button><a href="/panel/customers/projects/' + data[i].slug + '/budget/' + data[i].id + '/edit/" title="Editar"><button class="btn btn-success btn-icon"><i class="fa fa-edit"></i></button></a><button class="btn btn-warning btn-icon btn_create_control_budget" id="create_budget_control_' + data[i].id + '" title="Añadir progreso"><i class="fa fa-plus"></i></button><button class="btn btn-danger btn_delete_budget btn-icon" id="delete_budget_' + data[i].id + '" title="Eliminar"><i class="fa fa-trash"></i></button></td>'
            row += '</tr>'
            $('#kt_datatable').append(row)            
        }   
        $('#kt_datatable').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_budgets_control(id_budget){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/filter/budgets-control/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'id_budget': id_budget,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable_1_2').DataTable().destroy()
        $('#kt_datatable_1_2 tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].created_at_by + '</td>'
            row += '<td>' + data[i].item_budget + ' - ' + data[i].total_qqties + ' ' + data[i].measurementunit + ' - $' + data[i].total_item + ' ' + data[i].currency +'</td>'
            row += '<td>' + data[i].description + '</td>'
            row += '<td>' + data[i].earned_value + '</td>'
            row += '<td>' + data[i].executed_budget + '</td>'
            row += '<td><button class="btn btn-success btn_get_control_budget btn-icon" id="edit_budgets_control_' + data[i].id + '" title="Editar"><i class="fa fa-edit"></i></button><button class="btn btn-danger btn_delete_control_budget btn-icon" id="delete_control_budget_' + data[i].id + '" title="Eliminar"><i class="fa fa-trash"></i></button></td>'
            row += '</tr>'
            $('#kt_datatable_1_2').append(row)            
        }   
        $('#kt_datatable_1_2').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_all_activity_control_budget(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()
    $.ajax({
        url: "/panel/customers/projects/filter/all/budgets-control/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable_2').DataTable().destroy()
        $('#kt_datatable_2 tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].created_at_by + '</td>'
            row += '<td>' + data[i].item_budget + ' - ' + data[i].total_qqties + ' ' + data[i].measurementunit + ' - $' + data[i].total_item + ' ' + data[i].currency +'</td>'
            row += '<td>' + data[i].description + '</td>'
            row += '<td>' + data[i].earned_value + '</td>'
            row += '<td>' + data[i].executed_budget + '</td>'
            row += '</tr>'
            $('#kt_datatable_2').append(row)            
        }   
        $('#kt_datatable_2').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })

}

function project_load_statistics(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/load-statistics/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        if(data.planned_physical_progress != "" && data.planned_financial_progress != ""){
            $('.has_curve_1').removeClass('d-none')
            $('.has_curve_2').removeClass('d-none')

            $('.has_no_curve_1').addClass('d-none')
            $('.has_no_curve_2').addClass('d-none')

            $('#planned_physical_progress_1').width(data.planned_physical_progress.replace(',', '.'))
            $('#planned_physical_progress_2').html(data.planned_physical_progress)

            $('#planned_financial_progress_1').width(data.planned_financial_progress.replace(',', '.'))
            $('#planned_financial_progress_2').html(data.planned_financial_progress)
        }  
        $('#total_physical_progress_1').width(Number((data.total_physical_progress).toFixed(0)) + "%")
        $('#total_physical_progress_2').html(data.total_physical_progress + "%")
        $('#total_financial_progress_1').width(Number((data.total_financial_progress).toFixed(0)) + "%")
        $('#total_financial_progress_2').html(data.total_financial_progress + "%")
        $('#project_activities').html(data.activities)
        $('#project_budget').html(data.budget)
        $('#project_collaborators').html(data.collaborators)
        $('#project_documents').html(data.documents)
        $('#project_issues').html(data.issues)
        $('#total_budget').html("$ " + data.total_budget)
        $('#won_budget').html("$ " + data.won_budget)
        $('#spent_budget').html("$ " + data.spent_budget)
        $('#remaining_budget').html("$ " + data.remaining_budget)
        $('#cpi_budget').html(data.cpi_budget)
        $('#total_mh').html(data.total_mh)
        $('#mh_won').html(data.mh_won)
        $('#mh_spent').html(data.mh_spent)
        $('#mh_improductives').html(data.mh_improductives)
        $('#cpi').html(data.cpi)
        

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })

}