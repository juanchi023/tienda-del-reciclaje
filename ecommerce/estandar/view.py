from django.views.generic import View
from django.views.generic.edit import ModelFormMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.shortcuts import redirect


class LoginMixin(View):

    @method_decorator(login_required(login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(LoginMixin, self).dispatch(*args, **kwargs)


class InstanceMixin(ModelFormMixin, LoginMixin):

    def get_instance(self):
        return None

    def add_arguments(self):
        return None

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instanciating the form.
        """
        self.object = self.get_instance()
        kwargs = super(InstanceMixin, self).get_form_kwargs()
        arg = self.add_arguments()
        if arg:
            kwargs.update(self.add_arguments())
        return kwargs


class TestIfHasAccessMixin(UserPassesTestMixin):

    def test_func(self):
        project = self.get_object()
        try:
            user_access = ProjectAccess.objects.get(user=self.request.user.usercustomer, project=project)
            if user_access:
                return True
        except Exception:
            if (self.request.user.usercustomer.is_admin and project.company == self.request.user.usercustomer.company) or self.request.user.usercustomer.management == project.management:
                return True
        return False
