from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mystore_db_2021',
        'USER': 'mystore_user_2021',
        'PASSWORD': 'TiendaReciclaje2021@.',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
