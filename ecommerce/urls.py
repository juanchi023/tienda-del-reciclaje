from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('chaining/', include('smart_selects.urls')),
    path('', include('apps.admins.urls')),
    path('', include('apps.customers.urls')),
    path('', include('apps.home.urls')),
    path('', include('apps.store.urls')),
]
