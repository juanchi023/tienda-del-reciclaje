function list_versions(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/version/get/ajax/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable').DataTable().destroy()
        $('#kt_datatable tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].created_by_at + '</td>'
            row += '<td>' + data[i].title + '</td>'
            row += '<td>' + data[i].description + '</td>'
            row += '</tr>'
            $('#kt_datatable').append(row)            
        }   
        $('#kt_datatable').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_activities(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/get/ajax/activity/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable').DataTable().destroy()
        $('#kt_datatable tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].version + '</td>'
            row += '<td>' + data[i].code + '</td>'
            row += '<td>' + data[i].qqty_estimated + '</td>'
            row += '<td>' + data[i].executed_work + ' (' +  data[i].executed_work_percent + '%)</td>'
            row += '<td>' + data[i].item_budget + '</td>'
            row += '<td><button class="btn btn-primary btn-icon btn_details_activity" id="details_activity_' + data[i].id + '"><i class="fa fa-search"></i></button></td>'
            row += '</tr>'
            $('#kt_datatable').append(row)            
        }   
        $('#kt_datatable').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_all_activity_progress(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/all/activity-progress/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable_2').DataTable().destroy()
        $('#kt_datatable_2 tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].activity + '</td>'
            row += '<td>' + data[i].progress_date + '</td>'
            row += '<td>' + data[i].progress_quantity_work + '</td>'
            row += '<td>' + data[i].progress_man_hours + '</td>'
            row += '</tr>'
            $('#kt_datatable_2').append(row)            
        }   
        $('#kt_datatable_2').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_activity_progress(id_activity){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/filter/activity-progress/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'id_activity': id_activity,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable_1_2').DataTable().destroy()
        $('#kt_datatable_1_2 tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].progress_date + '</td>'
            row += '<td>' + data[i].progress_quantity_work + '</td>'
            row += '<td>' + data[i].progress_man_hours + '</td>'
            row += '</tr>'
            $('#kt_datatable_1_2').append(row)            
        }   
        $('#kt_datatable_1_2').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_budgets(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/list/ajax/budget/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){

        $('#kt_datatable').DataTable().destroy()
        $('#kt_datatable tbody').html("")

        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].title + '</td>'
            row += '<td>' + data[i].total_qtty + '</td>'
            row += '<td>$ ' + data[i].budget + '</td>'
            row += '<td>$ ' + data[i].spent_budget + '</td>'
            row += '<td>$ ' + data[i].remaining_budget + '</td>'
            row += '<td><button class="btn btn-primary btn-icon btn_details_budget" id="details_budget_' + data[i].id + '"><i class="fa fa-search"></i></button></td>'
            row += '</tr>'
            $('#kt_datatable').append(row)            
        }   
        $('#kt_datatable').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_budgets_control(id_budget){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/filter/budgets-control/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'id_budget': id_budget,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable_1_2').DataTable().destroy()
        $('#kt_datatable_1_2 tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].created_at_by + '</td>'
            row += '<td>' + data[i].item_budget + ' - ' + data[i].total_qqties + ' ' + data[i].measurementunit + ' - $' + data[i].total_item + ' ' + data[i].currency +'</td>'
            row += '<td>' + data[i].description + '</td>'
            row += '<td>' + data[i].earned_value + '</td>'
            row += '<td>' + data[i].executed_budget + '</td>'
            row += '</tr>'
            $('#kt_datatable_1_2').append(row)            
        }   
        $('#kt_datatable_1_2').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })
}

function list_all_activity_control_budget(){

    const id_project = $('#id_project').val()    
    const csrfmiddlewaretoken = $('input[name ="csrfmiddlewaretoken"]').val()

    $.ajax({
        url: "/panel/customers/projects/filter/all/budgets-control/", 
        type: "POST",
        dataType: "json",
        data: {
            'id_project': id_project,
            'csrfmiddlewaretoken': csrfmiddlewaretoken
        }
    }).done(function(data){
        $('#kt_datatable_2').DataTable().destroy()
        $('#kt_datatable_2 tbody').html("")
        for(let i=0; i< data.length; i++){
            let row = '<tr class="text-center">'
            row += '<td>' + data[i].created_at_by + '</td>'
            row += '<td>' + data[i].item_budget + ' - ' + data[i].total_qqties + ' ' + data[i].measurementunit + ' - $' + data[i].total_item + ' ' + data[i].currency +'</td>'
            row += '<td>' + data[i].description + '</td>'
            row += '<td>' + data[i].earned_value + '</td>'
            row += '<td>' + data[i].executed_budget + '</td>'
            row += '</tr>'
            $('#kt_datatable_2').append(row)            
        }   
        $('#kt_datatable_2').DataTable({
            responsive: true,
            paging: true,
            destroy: true,
        });       

    }).fail(function(data){
        console.log(data)
        Swal.fire({
            title: '¡Error!',
            text: data.responseJSON.error,
            icon: 'error',
            confirmButtonText: 'Ok'
        })
    })

}