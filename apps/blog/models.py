import os
from django.contrib.auth import get_user_model
from django.db import models
from projectdeps.estandar.models import BaseModel
from django.template.defaultfilters import slugify
from django.urls import reverse


class Category(BaseModel):

    name = models.CharField(max_length=40)
    icon = models.CharField(max_length=50)
    slug = models.SlugField(unique=True, null=True, blank=True, max_length=255)

    def __str__(self):
        return "{0}".format(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Categoría del blog'
        verbose_name_plural = 'Categorías del blog'


def upload_photo_consultant(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'consultores/{0}/{1}{2}'.format(instance.name, filename_base, filename_ext.lower())


class Consultant(BaseModel):

    name = models.CharField(max_length=250)
    photo = models.ImageField(upload_to=upload_photo_consultant, blank=True)
    bio = models.TextField(blank=True)
    slug = models.SlugField(unique=True, null=True, blank=True, max_length=255)

    def __str__(self):
        return "{0}".format(self.name)

    class Meta:
        verbose_name = 'Consultor'
        verbose_name_plural = 'Consultores'


def upload_imagen_post(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'blog/post/{0}/{1}{2}'.format(instance.id, filename_base, filename_ext.lower())


class Post(BaseModel):

    image = models.ImageField(upload_to=upload_imagen_post, blank=True)
    title = models.CharField(max_length=500, editable=True)
    categories = models.ManyToManyField(Category, blank=True)
    slug = models.SlugField(editable=True, unique=True, null=True, blank=True, max_length=400)
    views = models.BigIntegerField(default=1, null=True, blank=True)
    short_description = models.CharField(max_length=200)
    published = models.BooleanField(default=True)
    description = models.TextField(blank=True)
    consultant = models.ForeignKey(Consultant, on_delete=models.SET_NULL, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    def __str__(self):
        return "{0}".format(self.title)

    def get_absolute_url(self):
        return reverse('single_post', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = 'Publicación'
        verbose_name_plural = 'Publicaciones'


class Comment(BaseModel):

    user = models.CharField(max_length=500, blank=False)
    email = models.CharField(max_length=500, blank=False)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return "{0} - {1} - {2}".format(self.post.title, self.user, self.email)

    class Meta:
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'
