from django import template
from apps.blog.models import *

register = template.Library()


@register.filter
def get_count_posts(category):

    num = Post.objects.filter(categories=category).count()

    return num


@register.filter
def get_count_comment(post):

    num = Comment.objects.filter(post=post).count()

    return num


@register.filter
def get_num_posts(consultant):

    num = Post.objects.filter(consultant=consultant).count()

    return num
