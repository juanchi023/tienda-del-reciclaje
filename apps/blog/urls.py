from django.urls import path
from . import views


urlpatterns = [
    path('blog/', views.home_blog, name='home_blog'),
    path('blog/busqueda/', views.busqueda_palabra_clave, name='busqueda_palabra_clave'),
    path('blog/categoria/<slug:slug>/', views.filtro_category, name='filtro_category'),
    path('blog/post/<slug:slug>/', views.single_post, name='single_post'),
    path('blog/post-comment/', views.post_comment, name='post_comment'),
    # ============ Administradores ============
    # Publicaciones
    path('panel/admin/blog/gestion-posts/', views.gestion_post_admin, name='gestion_post_admin'),

    path('panel/admin/blog/gestion-post/publicar-post/', views.cambiar_estado_publicacion, name='cambiar_estado_publicacion'),

    path('panel/admin/blog/gestion-post/borrar-post/', views.borrar_publicacion, name='borrar_publicacion'),

    path('panel/admin/blog/gestion-post/nuevo-post/', views.PostCreation.as_view(), name='nueva_publicacion_admin'),

    path('panel/admin/blog/gestion-post/editar-post/<slug:slug>', views.PostUpdate.as_view(), name='editar_publicacion_admin'),

    # Categorías
    path('panel/admin/blog/gestion-categorias/', views.gestion_category_admin, name='gestion_category_admin'),

    path('panel/admin/blog/gestion-post/nueva-categoria/', views.nueva_category_admin, name='nueva_category_admin'),

    path('panel/admin/blog/gestion-post/editar-categoria/<slug:slug>/', views.editar_category_admin, name='editar_category_admin'),

    path('panel/admin/blog/gestion-post/borrar-categoria/', views.borrar_category, name='borrar_category'),

    # Consultores
    path('panel/admin/blog/gestion-consultores/', views.consultant_list_admin, name='consultant_list_admin'),

    path('panel/admin/blog/gestion-consultores/nuevo-consultor/', views.ConsultantCreation.as_view(), name='create_consultant_admin'),

    path('panel/admin/blog/gestion-consultores/editar-consultor/<int:pk>/', views.ConsultantUpdate.as_view(), name='edit_consultant_admin'),

    path('panel/admin/blog/gestion-consultores/borrar-consultor/', views.delete_consultant, name='delete_consultant'),

]
