from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.views.generic import ListView, DetailView
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.db.models import Q
from projectdeps.estandar.view import LoginMixin


def home_blog(request):
    publicaciones = Post.objects.filter(is_active=True).order_by('-created_at')
    paginator = Paginator(publicaciones, 5)

    recent_posts = publicaciones[:3]

    page = request.GET.get('page')
    posts = paginator.get_page(page)

    categories = Category.objects.all()

    return render(request, 'blog/blog_index.html', {'posts': posts, 'categories': categories, 'recent_posts': recent_posts})


def single_post(request, *args, **kwargs):
    recent_posts = Post.objects.all().order_by('created_at')[:3]
    post = Post.objects.get(slug=kwargs.get('slug'))
    categories_post = post.categories.all()

    categories = Category.objects.all()
    comments = Comment.objects.filter(post__slug=kwargs.get('slug'), is_active=True).order_by('-created_at')
    return render(request, 'blog/blog-single.html', {'post': post, 'categories': categories, 'recent_posts': recent_posts, 'comments': comments})


def busqueda_palabra_clave(request):

    categories = Category.objects.all()

    busqueda = request.GET.get('busqueda')

    publicaciones = Post.objects.filter(Q(title__icontains=busqueda) | Q(categories__name__icontains=busqueda)).distinct().order_by('-created_at')

    recent_posts = Post.objects.filter(is_active=True).order_by('-created_at')[:3]

    paginator = Paginator(publicaciones, 10)
    try:
        page = request.GET['page']
    except Exception:
        page = 1
    posts = paginator.get_page(page)

    return render(request, 'blog/word_filter.html', {'posts': posts, 'busqueda': busqueda, 'categories': categories, 'publicaciones': publicaciones, 'recent_posts': recent_posts})


def filtro_category(request, *args, **kwargs):

    category = Category.objects.get(slug=kwargs.get('slug'))

    cats = Category.objects.all()

    publicaciones = Post.objects.filter(categories__id=category.id).order_by('-created_at')

    recent_posts = Post.objects.filter(is_active=True).order_by('-created_at')[:3]

    paginator = Paginator(publicaciones, 10)

    try:
        page = request.GET['page']
    except Exception:
        page = 1
    posts = paginator.get_page(page)

    return render(request, 'blog/category_filter.html', {'posts': posts, 'category': category, 'recent_posts': recent_posts, 'categories': cats})


@require_POST
def post_comment(request, *args, **kwargs):
    if request.method == 'POST':
        email = request.POST.get('email', None)
        user = request.POST.get('user', None)
        mensaje = request.POST.get('comment', None)
        slug = request.POST.get('slug', None)

        comment = Comment()
        comment.user = user
        comment.email = email
        comment.description = mensaje
        comment.post = Post.objects.get(slug=slug)
        comment.save()

        message = 'Su comentario se ha enviado!'

        ctx = {'message': message}

        return HttpResponse(json.dumps(ctx), content_type='application/json')


def error_404(request):
    return render(request, 'home/blog/404.html')

# ================== Administradores ==================


# ______________ Categorías ______________
@login_required
def gestion_category_admin(request):

    categories = Category.objects.all()
    return render(request, 'panel/admin/blog/gestion-categorias.html', {'categories': categories})


@login_required
def nueva_category_admin(request):
    form = NewCategoryForm(request.POST or None)

    if form.is_valid():
        form.save()
        url = reverse('gestion_category_admin')
        return HttpResponseRedirect(url)

    return render(request, 'panel/admin/blog/nueva-category.html', {'form': form})


@login_required
def editar_category_admin(request, *args, **kwargs):

    category = Category.objects.get(slug=kwargs.get('slug'))

    form = EditCategoryForm(request.POST or None, instance=category)

    if form.is_valid():
        form.save()
        url = reverse('gestion_category_admin')
        return HttpResponseRedirect(url)

    return render(request, 'panel/admin/blog/editar-category.html', {'form': form})


@login_required
@require_POST
def borrar_category(request):

    message = ""

    if request.method == 'POST':

        id_cat = request.POST.get('id_cat', None)
        category = Category.objects.get(id=id_cat)

        category.delete()

        message = '¡La categoría fue eliminada exitosamente!'

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


# ______________ Publicaciones ______________
@login_required
def gestion_post_admin(request):

    publicaciones = Post.objects.all().order_by('-created_at')
    return render(request, 'panel/admin/blog/gestion-blog.html', {'publicaciones': publicaciones})


class PostCreation(LoginMixin, CreateView):
    form_class = NewPostForm
    template_name = 'panel/admin/blog/nuevo-post.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('editar_publicacion_admin', kwargs={'slug': self.object.slug})


class PostUpdate(LoginMixin, UpdateView):
    model = Post
    form_class = EditPostForm
    template_name = 'panel/admin/blog/editar-post.html'
    success_url = reverse_lazy('gestion_post_admin')

    def form_valid(self, form):
        form.instance.modified_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('editar_publicacion_admin', kwargs={'slug': self.object.slug})


@login_required
@require_POST
def borrar_publicacion(request):
    message = ""

    if request.method == 'POST':

        id_post = request.POST.get('id_post', None)
        publicacion = Post.objects.get(id=id_post)

        publicacion.delete()

        message = '¡La publicación fue eliminada exitosamente!'

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
@require_POST
def cambiar_estado_publicacion(request):
    message = ""

    if request.method == 'POST':

        id_post = request.POST.get('id_post', None)
        publicacion = Post.objects.get(id=id_post)

        if publicacion.is_active:
            publicacion.is_active = False
            publicacion.save()
            message = 'La publicacion se ocultó exitosamente'
        elif not publicacion.is_active:
            publicacion.is_active = True
            publicacion.save()
            message = 'La publicacion fue publicada exitosamente'

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


# ______________ Consultores ______________
@login_required
def consultant_list_admin(request):

    consultants = Consultant.objects.all().order_by('-created_at')
    return render(request, 'panel/admin/blog/consultant-list.html', {'consultants': consultants})


class ConsultantCreation(LoginMixin, CreateView):
    form_class = NewConsultantForm
    template_name = 'panel/admin/blog/new-consultant.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('edit_consultant_admin', kwargs={'pk': self.object.pk})


class ConsultantUpdate(LoginMixin, UpdateView):
    model = Consultant
    form_class = EditConsultantForm
    template_name = 'panel/admin/blog/edit-consultant.html'
    success_url = reverse_lazy('consultant_list_admin')

    def form_valid(self, form):
        form.instance.modified_by = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('consultant_list_admin')


@login_required
@require_POST
def delete_consultant(request):
    message = ""

    if request.method == 'POST':

        id_consultant = request.POST.get('id_consultant', None)

        try:
            consultant = Consultant.objects.get(id=id_consultant)

            posts = Post.objects.filter(consultant=consultant)

            if posts.count() == 0:
                consultant.delete()
                message = '¡El consultor fue eliminado exitosamente!'
            else:
                message = '¡El consultor tiene publicaciones asociadas, no es posible su eliminación!'

        except Exception as e:
            message = '¡Error - consulte con el administrador del sistema! {0}'.format(str(e))

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')
