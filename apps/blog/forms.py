from __future__ import unicode_literals
from django.forms import ModelForm
from django import forms
from .models import *


class NewPostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('image', 'title', 'categories', 'short_description', 'consultant', 'description')


class EditPostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('image', 'title', 'categories', 'short_description', 'consultant', 'description')


class NewCategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = ('name', 'icon')


class EditCategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = ('name', 'icon')


class NewConsultantForm(forms.ModelForm):

    class Meta:
        model = Consultant
        fields = ('photo', 'name', 'bio')


class EditConsultantForm(forms.ModelForm):

    class Meta:
        model = Consultant
        fields = ('photo', 'name', 'bio')
