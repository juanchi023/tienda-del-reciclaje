from django.contrib import admin
from .models import *


@admin.register(Post, Category, Comment, Consultant)
class BlogAdmin(admin.ModelAdmin):
    pass
