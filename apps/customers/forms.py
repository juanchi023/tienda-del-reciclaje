from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from .models import *
from apps.store.models import *


class CustomerRegisterForm(UserCreationForm):

    CHOICES_1 = (
        (1, 'Natural'),
        (2, 'Jurídica'),
    )

    CHOICES = (
        ('CC', 'Cédula de Ciudadanía'),
        ('PS', 'Pasaporte'),
        ('CE', 'Cédula de Extranjería'),
        ('NIT', 'NIT'),
        ('IX', 'NIT Extranjero'),
        ('OT', 'Otro')
    )


    email = forms.EmailField()
    phone = forms.CharField()
    user_type = forms.ChoiceField(choices=CHOICES_1)
    company_name = forms.CharField()     
    company_id_type = forms.ChoiceField(choices=CHOICES)
    company_id_number = forms.IntegerField()

    def __init__(self, *args, **kwargs):
        super(CustomerRegisterForm, self).__init__(*args, **kwargs)
        self.fields['company_name'].required = False
        self.fields['company_id_type'].required = False
        self.fields['company_id_number'].required = False

    def clean_email(self):
        data = self.cleaned_data['email']

        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("Este email ya lo tiene otro usuario")

        return data

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'phone',
        )


class CustomerEditForm(forms.ModelForm):
	
    email = forms.EmailField()
    first_name = forms.CharField()
    last_name = forms.CharField()
    birth_date = forms.DateField(
        widget=forms.DateInput(format='%Y-%m-%d'),
        input_formats=('%Y-%m-%d', )
    )


    def clean_email(self):
        data = self.cleaned_data['email']
        
        users = User.objects.filter(email__iexact=data).exclude(email__iexact=data)

        if users:
            raise forms.ValidationError("Este email ya lo tiene otro usuario")

        return data
	
    def clean_profile_img(self):
        profile_img = self.cleaned_data.get('profile_img', False)
        if profile_img:
            if profile_img.size > 4*1024*1024:
                raise forms.ValidationError("Imagen muy pesada ( > 4mb )")
            return profile_img

    class Meta:
        model = Customer
        fields = (
            'first_name',
            'last_name',
            'email',
            'phone',
            'id_type',
            'id_number',
            'profile_img',
            'birth_date',
        )


class DirectionCreateForm(forms.ModelForm):

    class Meta:
        model = Direction
        fields = (
            'address',
            'reference',
            'zip_code',
            'phone',
        )


class SellerCreateForm(forms.ModelForm):

    class Meta:
        model = CompanyProfile
        fields = (
            'name',
            'email',
            'address',
            'zip_code',
            'phone',
            'website_url',
            'logo',
            'cover_img',
            'id_type',
            'id_number',
        )


class StoreProfileEditForm(forms.ModelForm):
    
    class Meta:
        model = CompanyProfile
        fields = (
            'name',
            'address',
            'zip_code',
            'phone',
            'website_url',
            'logo',
            'cover_img',
        )


class StoreProductCreateForm(forms.ModelForm):
    

    class Meta:
        model = Product
        fields = (
            'title',
            'category',
            'description',
            'main_img',
            'sku',
            'unit_price',
            'discount_price',
            'quantity_per_unit',
            'available_units',
            'in_bid',
            'bid_price_start_at',
            'bid_date_start',
            'bid_date_end',
            'required_documents',
        )
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].queryset = ProductCategory.objects.exclude(is_active=False)


class StoreProductEditForm(forms.ModelForm):

    bid_date_start = forms.DateField(
        widget=forms.DateInput(format='%Y-%m-%d'),
        input_formats=('%Y-%m-%d', ),
        required=False
    )

    bid_date_end = forms.DateField(
        widget=forms.DateInput(format='%Y-%m-%d'),
        input_formats=('%Y-%m-%d', ),
        required=False
    )
    

    class Meta:
        model = Product
        fields = (
            'title',
            'category',
            'description',
            'main_img',
            'sku',
            'unit_price',
            'discount_price',
            'quantity_per_unit',
            'in_bid',
            'bid_price_start_at',
            'bid_date_start',
            'bid_date_end',
            'required_documents',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].queryset = ProductCategory.objects.exclude(is_active=False)
