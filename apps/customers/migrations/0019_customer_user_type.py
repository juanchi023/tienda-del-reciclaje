# Generated by Django 3.1.5 on 2021-03-13 22:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0018_companyprofile_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='user_type',
            field=models.CharField(blank=True, choices=[('Natural', 'Natural'), ('Jurídica', 'Jurídica')], max_length=25),
        ),
    ]
