from django.urls import path, include
from apps.customers import views as customers_views

urlpatterns = [
    path('clientes/panel/', customers_views.customers_panel_view, name='customers_panel_view'),

    path('clientes/perfil/', customers_views.customers_profile_view, name='customers_profile_view'),

    path('clientes/direcciones/', customers_views.customers_directions_view, name='customers_directions_view'),

    path('clientes/direcciones/get/', customers_views.get_direction, name='get_direction'),

    path('clientes/direcciones/get/all/', customers_views.get_all_directions, name='get_all_directions'),

    path('clientes/direcciones/get/city/', customers_views.city_filter_directions, name='city_filter_directions'),

    path('clientes/direcciones/edit/', customers_views.edit_direction, name='edit_direction'),

    path('clientes/direcciones/delete/', customers_views.delete_direction, name='delete_direction'),

    path('clientes/lista-de-deseos/', customers_views.customers_wishlist_view, name='customers_wishlist_view'),

    path('clientes/lista-de-deseos/add-product/', customers_views.customers_add_product_wishlist, name='customers_add_product_wishlist'),
    
    path('clientes/lista-de-deseos/delete-product/', customers_views.customer_delete_from_wishlist, name='customer_delete_from_wishlist'),

    path('clientes/lista-de-deseos/clear-wishlist/', customers_views.customer_clear_all_wishlist, name='customer_clear_all_wishlist'),

    # Cart
    path('clientes/carrito/', customers_views.cart_view, name='cart_view'),

    path('clientes/agregar-al-carrito/', customers_views.customers_add_to_cart, name='customers_add_to_cart'),

    path('clientes/agregar-al-carrito-detallado/', customers_views.customers_add_to_cart_detailed, name='customers_add_to_cart_detailed'),

    path('clientes/actualizar-carrito/', customers_views.customers_update_cart_product, name='customers_update_cart_product'),

    path('clientes/eliminar-del-carrito/', customers_views.customers_remove_cart_product, name='customers_remove_cart_product'),

    path('clientes/vacia-carrito/', customers_views.customers_clear_cart, name='customers_clear_cart'),

    # Join Sellers
    path('clientes/vende-con-nosotros/', customers_views.customers_join_sellers_view, name='customers_join_sellers_view'),


    # Stores Profile    
    path('tiendas/panel/', customers_views.StorePanelView.as_view(), name='stores_panel_view'),

    path('tiendas/panel/editar-perfil/', customers_views.stores_profile_edit, name='stores_profile_edit'),

    # Products

    path('tiendas/panel/productos/', customers_views.stores_products_list, name='stores_products_list'),

    path('tiendas/panel/productos/nuevo/', customers_views.stores_products_create, name='stores_products_create'),

    path('tiendas/panel/productos/<pk>/detalles/', customers_views.stores_products_details, name='stores_products_details'),

    path('tiendas/panel/productos/agregar-unidades/', customers_views.stores_add_stock_product, name='stores_add_stock_product'),

    path('tiendas/panel/productos/coultar/', customers_views.stores_hide_product, name='stores_hide_product'),

    path('tiendas/panel/productos/<pk>/editar/', customers_views.stores_products_edit, name='stores_products_edit'),

]
