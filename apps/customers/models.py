import os
from django.db import models
from django.contrib.auth.models import User
from ecommerce.estandar.models import BaseModel
from django.template.defaultfilters import slugify


class Country(BaseModel):
    
    name = models.CharField(max_length=150)
    short_name = models.CharField(max_length=5)

    class Meta:
        verbose_name = 'País'
        verbose_name_plural = 'Paises'

    def __str__(self):
        return "{0}".format(self.name)


class City(BaseModel):
    
    name = models.CharField(max_length=150)
    short_name = models.CharField(max_length=5)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'

    def __str__(self):
        return "{0}".format(self.name)


def upload_img(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'customers/{0}/{1}{2}'.format(instance.user.username, filename_base, filename_ext.lower())


class Customer(BaseModel):

    TYPE_USER = (
        ('Natural', 'Natural'),
        ('Jurídica', 'Jurídica'),
    )

    ID_T = (
        ('CC', 'Cédula de Ciudadanía'),
        ('PS', 'Pasaporte'),
        ('CE', 'Cédula de Extranjería'),
        ('OT', 'Otro')
    )

    COMPANY_ID_T = (
        ('CC', 'Cédula de Ciudadanía'),
        ('PS', 'Pasaporte'),
        ('CE', 'Cédula de Extranjería'),
        ('NIT', 'NIT'),
        ('IX', 'NIT Extranjero'),
        ('OT', 'Otro')
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='customer')
    id_type = models.CharField(max_length=25, choices=ID_T, blank=True)
    id_number = models.CharField(max_length=25, null=True, blank=True)
    user_type = models.CharField(max_length=25, choices=TYPE_USER, blank=True)
    phone = models.CharField(max_length=15, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)
    profile_img = models.ImageField(upload_to=upload_img, blank=True, null=True)
    
    is_company = models.BooleanField(default=False)
    company_name = models.CharField(max_length=150, blank=True)
    company_id_type = models.CharField(max_length=25, choices=COMPANY_ID_T, blank=True)
    company_id_number = models.CharField(max_length=25, null=True, blank=True)

    points = models.BigIntegerField(default=0)

    email_confirmed = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def __str__(self):
        return "{} - {}".format(
            self.user.email,
            self.get_full_name()
        )

    def get_full_name(self):
        return "{} {}".format(self.user.first_name, self.user.last_name)


class Direction(BaseModel):

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='directions', blank=True, null=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)
    address = models.CharField(max_length=250, blank=True)
    reference = models.CharField(max_length=250, blank=True)
    zip_code = models.CharField(max_length=20, blank=True, null=True)
    phone = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        verbose_name = 'Dirección de Cliente'
        verbose_name_plural = 'Direcciones de Clientes'

    def __str__(self):
        return "Dirección de cliente {} - {}, {}".format(
            self.customer.user.username,
            self.address,
            self.city
        )


def upload_company_logo(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'customers/{0}/company/images/{1}{2}'.format(instance.customer.user.username, filename_base, filename_ext.lower())


class CompanyProfile(BaseModel):

    ID_T = (
        ('CC', 'Cédula de Ciudadanía'),
        ('PS', 'Pasaporte'),
        ('CE', 'Cédula de Extranjería'),
        ('NIT', 'NIT'),
        ('IX', 'NIT Extranjero'),
        ('OT', 'Otro')
    )

    customer = models.OneToOneField(Customer, on_delete=models.CASCADE, related_name='store', blank=True, null=True)
    name = models.CharField(max_length=150, blank=True)
    id_type = models.CharField(max_length=25, choices=ID_T, blank=True)
    id_number = models.CharField(max_length=250, null=True, blank=True)
    address = models.CharField(max_length=250, blank=True)
    email = models.CharField(max_length=250, blank=True)
    zip_code = models.CharField(max_length=20, blank=True, null=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    website_url = models.URLField(max_length=250, null=True, blank=True)
    logo = models.ImageField(upload_to=upload_company_logo, blank=True, null=True)
    cover_img = models.ImageField(upload_to=upload_company_logo, blank=True, null=True)
    slug = models.SlugField(unique=True, null=True, blank=True, max_length=255)

    is_official = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Tienda'
        verbose_name_plural = 'Tiendas'

    def __str__(self):
        return "{0}".format(self.name)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)
        super(CompanyProfile, self).save(*args, **kwargs)
