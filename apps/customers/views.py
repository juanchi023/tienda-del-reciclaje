from django.shortcuts import render, get_object_or_404
from django.views.generic.base import TemplateView
from django.views.generic import DetailView
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .models import *
from apps.store.models import *
from django.urls import reverse_lazy
from django.contrib import messages
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMultiAlternatives
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import *
from django.contrib.auth.mixins import LoginRequiredMixin
from datetime import datetime
from django.utils import timezone
from django.contrib.humanize.templatetags.humanize import intcomma


@login_required
def customers_panel_view(request):
    
    customer = request.user.customer

    wishlist = FavoriteProduct.objects.filter(customer=customer)
    directions = Direction.objects.filter(customer=customer)
    return render(request, 'customers/panel.html', {
        'directions': directions,
        'wishlist': wishlist,
    })


@login_required
def customers_profile_view(request):

    customer = request.user.customer

    wishlist = FavoriteProduct.objects.filter(customer=customer)
    form = CustomerEditForm(request.POST or None, request.FILES or None, instance=customer)
    countries = Country.objects.all().order_by('-name')
    directions = Direction.objects.filter(customer=customer)

    if request.method == 'POST':
        
        id_city = request.POST['id_city']
        city = City.objects.get(id=id_city)

        if form.is_valid():
            customer = form.save()
            customer.city = city
            customer.save()
            user = customer.user
            user.email = request.POST.get('email')
            user.first_name = request.POST.get('first_name')
            user.last_name = request.POST.get('last_name')
            user.save()
            messages.success(request, '¡Datos actualizados exitosamente!')
            return HttpResponseRedirect(reverse('customers_panel_view'))

    return render(request, 'customers/profile.html', {
        'form': form,
        'countries': countries,
        'directions': directions,
        'wishlist': wishlist,
    })


# Customer Directions
@login_required
def customers_directions_view(request):

    customer = request.user.customer

    wishlist = FavoriteProduct.objects.filter(customer=customer)
    directions = Direction.objects.filter(customer=customer)
    countries = Country.objects.all().order_by('-name')
    form = DirectionCreateForm(request.POST or None)

    if request.method == 'POST':
        
        id_city = request.POST['id_city']
        city = City.objects.get(id=id_city)

        if form.is_valid():
            direction = form.save()
            direction.city = city
            direction.customer = customer
            direction.save()
            messages.success(request, '¡Dirección creada exitosamente!')
            return HttpResponseRedirect(reverse('customers_directions_view'))

    return render(request, 'customers/directions.html', {
        'form': form,
        'directions': directions,
        'countries': countries,
        'wishlist': wishlist,
    })


@login_required
@require_POST
def get_direction(request):

    direction_1 = {}

    if request.method == 'POST':

        id_direction = request.POST.get('id_direction', None)

        direction = Direction.objects.get(id=id_direction)
        
        if request.user.customer == direction.customer:
            direction_1 = {
                'id': direction.id,
                'customer_id': direction.customer.id,
                'city_id': direction.city.id,
                'city_name': direction.city.name,
                'country_id': direction.city.country.id,
                'country_name': direction.city.country.name,
                'address': direction.address,
                'reference': direction.reference,
                'zip_code': direction.zip_code,
                'phone': direction.phone,
            }
            return HttpResponse(json.dumps(direction_1), content_type='application/json')            
        else:
            message = "Prohibido"
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response


@login_required
@require_POST
def get_all_directions(request):

    list_directions = []

    if request.method == 'POST':

        id_customer = request.POST.get('id_customer', None)

        directions = Direction.objects.filter(customer__id=id_customer).order_by('-created_at')
        
        if request.user.customer:
            for direction in directions:
                
                direction_1 = {
                    'id': direction.id,
                    'customer_id': direction.customer.id,
                    'city_id': direction.city.id,
                    'city_name': direction.city.name,
                    'country_id': direction.city.country.id,
                    'country_name': direction.city.country.name,
                    'address': direction.address,
                    'reference': direction.reference,
                    'zip_code': direction.zip_code,
                    'phone': direction.phone,
                }
                list_directions.append(direction_1)
            return HttpResponse(json.dumps(list_directions), content_type='application/json')            
        else:
            message = "Prohibido"
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response


@login_required
@require_POST
def city_filter_directions(request):

    list_cities = []

    if request.method == 'POST':
        id_country = request.POST.get('id_country', None)
        id_direction = request.POST.get('id_direction', None)

        country = Country.objects.get(id=id_country)
        direction = Direction.objects.get(id=id_direction)

        cities = City.objects.filter(country=country)

        if request.user.customer:
            for city in cities:
                if direction.city == city:
                    list_cities.append({
                        'id': city.id,
                        'name': city.name,
                        'selected': True
                    })
                else:
                    list_cities.append({
                        'id': city.id,
                        'name': city.name,
                        'selected': False
                    })
        else:        
            for city in cities:
                list_cities.append({
                    'id': city.id,
                    'name': city.name
                })

        return HttpResponse(json.dumps(list_cities), content_type='application/json')


@login_required
@require_POST
def edit_direction(request):

    if request.method == 'POST':

        id_direction = request.POST.get('id_direction', None)

        direction = Direction.objects.get(id=id_direction)
        
        if request.user.customer == direction.customer:

            id_city = request.POST.get('id_city', None)
            address = request.POST.get('address', None)
            reference = request.POST.get('reference', None)
            zip_code = request.POST.get('zip_code', None)
            phone = request.POST.get('phone', None)

            city = City.objects.get(id=id_city)
            
            direction.city = city
            direction.address = address
            direction.reference = reference
            direction.zip_code  = zip_code
            direction.phone = phone
            direction.save()
            
            message = '¡Dirección modificada exitosamente!'
            ctx = {'message': message}
            return HttpResponse(json.dumps(ctx), content_type='application/json')         
        else:
            message = "Prohibido"
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response


@login_required
@require_POST
def delete_direction(request):

    if request.method == 'POST':

        id_direction = request.POST.get('id_direction', None)

        direction = Direction.objects.get(id=id_direction)
        
        if request.user.customer == direction.customer:
            direction.delete()
            
            message = '¡Dirección eliminada exitosamente!'
            ctx = {'message': message}
            return HttpResponse(json.dumps(ctx), content_type='application/json')         
        else:
            message = "Prohibido"
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response


# Customers Join Sellers
@login_required
def customers_join_sellers_view(request):

    customer = request.user.customer

    store = ""

    try:
        store = CompanyProfile.objects.get(customer=customer)
    
    except Exception:
        pass

    form = SellerCreateForm(request.POST or None, request.FILES or None)
    directions = Direction.objects.filter(customer=customer)
    wishlist = FavoriteProduct.objects.filter(customer=customer)

    if request.method == 'POST':

        if form.is_valid():
            store_1 = form.save()
            store_1.customer = request.user.customer
            store_1.save()
            messages.success(request, '¡Solicitud enviada exitosamente! En breve la revisaremos y te activaremos.')
            return HttpResponseRedirect(reverse('customers_join_sellers_view'))

    return render(request, 'customers/join_sellers.html', {
        'form': form,
        'store': store,
        'directions': directions,
        'wishlist': wishlist,
    })


@login_required
def customers_wishlist_view(request):
    
    customer = request.user.customer

    wishlist = FavoriteProduct.objects.filter(customer=customer)
    directions = Direction.objects.filter(customer=customer)

    return render(request, 'customers/wishlist.html', {'wishlist': wishlist, 'directions': directions})


@login_required
@require_POST
def customers_add_product_wishlist(request):

    if request.method == 'POST':

        id_product = request.POST.get('id_product', None)

        try:
        
            if request.user.customer:
                product = Product.objects.get(id=id_product)
                try:                     
                    fav = FavoriteProduct.objects.get(product=product)
                    if fav:
                        message = '¡Ya este producto lo tienes en tu lista de deseos!'

                except Exception:
                    fav = FavoriteProduct()
                    fav.customer = request.user.customer
                    fav.product = product
                    fav.save()                
                    message = '¡Producto agregado a tu lista de deseos exitosamente!'
                
                ctx = {'message': message}
                return HttpResponse(json.dumps(ctx), content_type='application/json')  

            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

def customer_delete_from_wishlist(request):

    if request.method == 'POST':

        id_fav = request.POST.get('id_fav', None)

        try:
            if request.user.customer:

                prod_fav = FavoriteProduct.objects.get(id=id_fav)
                prod_fav.delete()
                message = "Producto eliminado de la lista de deseos"                       
                ctx = {'message': message}
                return HttpResponse(json.dumps(ctx), content_type='application/json')
            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response                

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response


def customer_clear_all_wishlist(request):

    if request.method == 'POST':

        try:
            if request.user.customer:
                
                favs = FavoriteProduct.objects.filter(customer=request.user.customer)
                for fav in favs:
                    fav.delete()
                message = "Tu lista de deseos ha sido vaciada"                       
                ctx = {'message': message}
                return HttpResponse(json.dumps(ctx), content_type='application/json')
            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response                

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response


# Stores
class StorePanelView(LoginRequiredMixin, TemplateView):
    template_name = "panel/stores/panel.html"

    def get_context_data(self, **kwargs):
        context = super(StorePanelView, self).get_context_data(**kwargs)
        customer = self.request.user.customer
        context['products'] = Product.objects.filter(seller=customer)
        return context


@login_required
def stores_profile_edit(request, *args, **kwargs):

    store = request.user.customer.store

    form = StoreProfileEditForm(request.POST or None, request.FILES or None, instance=store)

    if request.method == 'POST':
        if form.is_valid():
            store = form.save()
            messages.success(
                request, 'Datos modificados exitosamente'
            )
            return HttpResponseRedirect(reverse('stores_panel_view'))

    return render(request, 'panel/stores/profile/stores_profile_edit.html', {'store': store, 'form': form})


# Products
@login_required
def stores_products_list(request, *args, **kwargs):

    customer = request.user.customer
    
    products = Product.objects.filter(seller=customer)

    return render(request, 'panel/stores/products/products_list.html', {'products': products, 'customer': customer})


@login_required
def stores_products_create(request, *args, **kwargs):

    customer = request.user.customer

    form = StoreProductCreateForm(request.POST or None, request.FILES or None)

    if request.method == 'POST':
        if form.is_valid():
            product = form.save(commit=False)            
            product.seller = customer
            product.is_active = True
            product.status = 0
            product.save()

            product_action = ProductAction()
            product_action.created_by = request.user
            product_action.product = product
            product_action.action = "Creación"
            now = datetime.now()
            product_action.details = "El producto fue creado por el usuario '{}' y pasó al estado PENDIENTE. Fecha {}".format(request.user, now.strftime("%d/%m/%Y %I:%M %p"))
            product_action.save()
                        
            messages.success(
                request, 'Producto creado exitosamente y en espera de aprobación'
            )
            url = reverse('stores_products_details', kwargs={'pk': product.id})
            return HttpResponseRedirect(url)

    return render(request, 'panel/stores/products/products_create.html', {'form': form, 'customer': customer})


@login_required
def stores_products_details(request, *args, **kwargs):

    try:

        product = Product.objects.get(pk=kwargs['pk'])

        if product.seller == request.user.customer:
            history = ProductAction.objects.filter(product=product).order_by('-created_at')

            return render(request, 'panel/stores/products/products_detail.html', {'product': product, 'history': history})
        else:
            messages.success(
                request, 'Acceso denegado'
            )
            return HttpResponseRedirect(reverse('stores_panel_view'))
    except Product.DoesNotExist:
        messages.success(
            request, 'Producto no encontrado'
        )
        return HttpResponseRedirect(reverse('stores_panel_view'))


@require_POST
@login_required
def stores_add_stock_product(request):

    message = ""

    if request.method == 'POST':
        id_product = request.POST.get('id_product', None)
        qqty_stock = request.POST.get('qqty_stock', None)
        try:
            product = Product.objects.get(id=id_product)

            old_stock = product.available_units

            if product.seller == request.user.customer:
                
                product.available_units += int(qqty_stock)
                product.save()

                message = '¡Nuevas existencias agregadas al producto exitosamente!'
                product_action = ProductAction()
                product_action.created_by = request.user
                product_action.product = product
                product_action.action = "Adición"
                now = datetime.now()
                product_action.details = "El producto pasó de tener {} unidades a {}. Fecha {}".format(old_stock, product.available_units, now.strftime("%d/%m/%Y %I:%M %p"))
                product_action.save()
            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@require_POST
@login_required
def stores_hide_product(request):

    status = 0

    message = ""

    if request.method == 'POST':
        id_product = request.POST.get('id_product', None)
        try:
            product = Product.objects.get(id=id_product)

            if product.seller == request.user.customer:
                status = product.status
                product.status = 0
                product.save()

                if status == 0:
                    status = "Pendiente"
                elif status == 1:
                     status =  "Publicado"
                elif status == 2:
                    status = "Rechazado"
                elif status == 3:
                    status = "Pausado"

                message = '¡Producto pasó a estar oculto!'
                product_action = ProductAction()
                product_action.created_by = request.user
                product_action.product = product
                product_action.action = "Ocultar"
                now = datetime.now()
                product_action.details = "El producto pasó de estar en estado {} a estar OCULTO. Fecha {}".format(status, now.strftime("%d/%m/%Y %I:%M %p"))
                product_action.save()
                # Enviar email a administrador avisándole la acción
            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
def stores_products_edit(request, *args, **kwargs):

    try:

        product = Product.objects.get(pk=kwargs['pk'])

        if product.seller == request.user.customer:

            form = StoreProductEditForm(request.POST or None, request.FILES or None, instance=product)

            if request.method == 'POST':
                if form.is_valid():
                    product = form.save()
                    url = reverse('stores_products_details', kwargs={'pk': product.id})
                    return HttpResponseRedirect(url)

            return render(request, 'panel/stores/products/products_edit.html', {'product': product, 'form': form})

        else:
            messages.success(
                request, 'Acceso denegado'
            )
            return HttpResponseRedirect(reverse('stores_panel_view'))
    
    except Product.DoesNotExist:
        messages.success(
            request, 'Producto no encontrado'
        )
        return HttpResponseRedirect(reverse('stores_panel_view'))


# Add products to cart
@login_required
@require_POST
def customers_add_to_cart(request):

    id_product = request.POST.get('id_product', None)
    product = get_object_or_404(Product, id=id_product)

    order_item, created = OrderItem.objects.get_or_create(
        product=product,
        customer=request.user.customer,
        ordered=False
    )
    order_qs = Order.objects.filter(customer=request.user.customer, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.products.filter(product__id=product.id).exists():
            order_item.quantity += 1
            order_item.save()
            message = "La cantidad de este producto en el carrito fue actualizada exitosamente"
            ctx = {'message': message}
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        else:
            order.products.add(order_item)
            message = "El producto fue agregado al carrito exitosamente"
            ctx = {'message': message}
            return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(
            customer=request.user.customer, ordered_date=ordered_date)
        order.products.add(order_item)
        message = "El producto fue agregado al carrito exitosamente"
        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
@require_POST
def customers_add_to_cart_detailed(request):

    id_product = request.POST.get('id_product', None)
    qtty = request.POST.get('qtty', None)
    product = get_object_or_404(Product, id=id_product)

    order_item, created = OrderItem.objects.get_or_create(
        product=product,
        customer=request.user.customer,
        ordered=False
    )

    order_qs = Order.objects.filter(customer=request.user.customer, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.products.filter(product__id=product.id).exists():
            order_item.quantity += int(qtty)
            order_item.save()
            message = "La cantidad de este producto en el carrito fue actualizada exitosamente"
            ctx = {'message': message}
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        else:
            order.products.add(order_item)
            message = "El producto fue agregado al carrito exitosamente"
            ctx = {'message': message}
            return HttpResponse(json.dumps(ctx), content_type='application/json')
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(
            customer=request.user.customer,
            ordered_date=ordered_date,
            quantity=int(qtty)
        )
        order.products.add(order_item)
        message = "El producto fue agregado al carrito exitosamente"
        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@login_required
def cart_view(request, *args, **kwargs):

    order, created = Order.objects.get_or_create(
        customer=request.user.customer,
        ordered=False
    )
    return render(request, 'home/cart.html', {'order': order})


@login_required
@require_POST
def customers_update_cart_product(request):

    id_product = request.POST.get('id_product', None)
    qtty = request.POST.get('qtty', None)
    try:
        product = get_object_or_404(Product, id=id_product)

        order = Order.objects.get(
            customer=request.user.customer,
            ordered=False
        )

        order_item = OrderItem.objects.get(
            product=product,
            customer=request.user.customer,
            ordered=False
        )    
        order_item.quantity = int(qtty)
        order_item.save()
        message = "La cantidad de este producto en el carrito fue actualizada exitosamente"
        ctx = {
            'message': message,
            'subtotal': intcomma(order_item.get_total_product_price()),
            'order_total': intcomma(order.get_total()),
            'reward_points': intcomma(order.get_total_reward_points())
        }
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    except Exception as e:
        message = str(e)
        response = JsonResponse({"error": message})
        response.status_code = 409
        return response


@login_required
@require_POST
def customers_remove_cart_product(request):

    id_product = request.POST.get('id_product', None)
    try:
        product = get_object_or_404(Product, id=id_product)

        order = Order.objects.get(
            customer=request.user.customer,
            ordered=False
        )

        order_item = OrderItem.objects.get(
            product=product,
            customer=request.user.customer,
            ordered=False
        )   

        order.products.remove(order_item)
        order_item.delete()
        message = "El producto fue sacado del carrito exitosamente"
        ctx = {
            'message': message,
            'order_total': intcomma(order.get_total()),
            'reward_points': intcomma(order.get_total_reward_points())
        }
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    except Exception as e:
        message = str(e)
        response = JsonResponse({"error": message})
        response.status_code = 409
        return response


@login_required
@require_POST
def customers_clear_cart(request):

    id_order = request.POST.get('id_order', None)
    try:

        order = Order.objects.get(
            customer=request.user.customer,
            id=id_order
        )

        for order_item in order.products.all():
            order.products.remove(order_item)
            order_item.delete()
        
        message = "El carrito fue vaciado exitosamente"
        ctx = {
            'message': message,
        }
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    except Exception as e:
        message = str(e)
        response = JsonResponse({"error": message})
        response.status_code = 409
        return response
