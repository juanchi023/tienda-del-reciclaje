from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic import DetailView
from datetime import datetime
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .forms import *
from django.urls import reverse_lazy
from django.contrib import messages
from apps.customers.models import *
from apps.store.models import *
from apps.home.views import send_email_to_customer
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.sites.shortcuts import get_current_site

import requests


class AdminsPanelView(LoginRequiredMixin, TemplateView):
    template_name = "panel/admins/panel.html"

    def get_context_data(self, **kwargs):
        context = super(AdminsPanelView, self).get_context_data(**kwargs)
        context['product_categories'] = ProductCategory.objects.all()
        context['products'] = Product.objects.all()
        context['customers'] = Customer.objects.all()
        context['stores'] = CompanyProfile.objects.all()
        return context


@login_required
def AdminsLogoutView(request):
    logout(request)
    return HttpResponseRedirect(reverse('admins_login'))


# ====================== Customers ======================
class CustomersListView(LoginRequiredMixin, TemplateView):
    template_name = "panel/admins/customers/customers_list.html"

    def get_context_data(self, **kwargs):
        context = super(CustomersListView, self).get_context_data(**kwargs)
        context['customers'] = Customer.objects.all().order_by('-user__date_joined')
        return context


@login_required
def admin_custormer_create(request):

    form = AdminCustomerCreateForm(request.POST or None, request.FILES or None)

    countries = Country.objects.all().order_by('-name')

    if request.method == 'POST':

        id_city = request.POST['id_city']
        phone = request.POST['phone']

        city = City.objects.get(id=id_city)

        if form.is_valid():
            user = form.save()
            user.username = request.POST['email']
            user.is_active = False
            user.save()
            customer = Customer()
            customer.user = user
            customer.city = city
            customer.phone = phone
            customer.save()
            site = get_current_site(request)
            current_site = site.domain
            send_email_to_customer(customer.id, request.POST['password1'], current_site)
            messages.success(
                request, 'Cliente creado exitosamente'
            )
            url = reverse('admins_customers_detailview', kwargs={'pk': customer.id})
            return HttpResponseRedirect(url)

    return render(request, 'panel/admins/customers/customers_create.html', {'form': form, 'countries': countries})


class CustomersDetailView(LoginRequiredMixin, DetailView):

    context_object_name = 'customer'
    template_name = 'panel/admins/customers/customers_detail.html'

    def get_object(self):
        id_customer = self.kwargs['pk']
        customer = Customer.objects.get(pk=id_customer)
        return customer

    def get_context_data(self, **kwargs):
        context = super(CustomersDetailView, self).get_context_data(**kwargs)
        customer = self.get_object()
        try:
            context['store'] = customer.store
        except Exception:
            pass

        context['customer'] = customer
        return context


@login_required
def admin_customer_edit(request, *args, **kwargs):

    customer = Customer.objects.get(pk=kwargs['pk'])

    countries = Country.objects.all().order_by('-name')

    form = AdminCustomerEditForm(request.POST or None, request.FILES or None, instance=customer)

    if request.method == 'POST':

        id_city = request.POST.get('id_city', None)

        if form.is_valid():
            customer = form.save(commit=False)
            customer.city = City.objects.get(id=id_city)
            customer.save()
            user = customer.user
            user.first_name = request.POST.get('first_name', None)
            user.last_name = request.POST.get('last_name', None)
            user.email = request.POST.get('email', None)
            user.save()
            messages.success(
                request, 'Datos del cliente modificados exitosamente'
            )
            url = reverse('admins_customers_detailview', kwargs={'pk': customer.id})
            return HttpResponseRedirect(url)

    return render(request, 'panel/admins/customers/customers_edit.html', {'form': form, 'countries':countries})


@require_POST
@login_required
def admin_change_customer_state(request):

    message = ""

    if request.method == 'POST':
        id_customer = request.POST.get('id_customer', None)
        try:
            customer = Customer.objects.get(id=id_customer)

            if customer.is_active:
                customer.is_active = False
                customer.save()
                message = '¡Se ha desactivado el usuario!'
            else:
                customer.is_active = True
                message = '¡Se ha activado el usuario!'
                customer.save()
        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@require_POST
@login_required
def admin_get_customer_details(request):

    message = ""
    profile_img_url = ""

    if request.method == 'POST':
        id_customer = request.POST.get('id_customer', None)
        try:
            customer = Customer.objects.get(id=id_customer)

            if customer.profile_img:
                profile_img_url = customer.profile_img.url

            data = {
                'username': customer.user.username,
                'email': customer.user.email,
                'full_name': customer.get_full_name(),
                'first_name': customer.user.first_name,
                'last_name': customer.user.last_name,
                'country': customer.city.country.name,
                'city': customer.city.name,
                'phone': customer.phone,
                'id_type': customer.id_type,
                'id_number': customer.id_number,
                'birth_date': customer.birth_date.strftime("%d/%m/%Y"),
                'is_company': customer.is_company,
                'profile_img_url': profile_img_url,
            }
            return HttpResponse(json.dumps(data), content_type='application/json')
            
        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response


@require_POST
@login_required
def admin_change_pswd_customer(request):

    message = ""

    if request.method == 'POST':
        if request.user.is_staff:
            id_customer = request.POST.get('id_customer', None)
            pswd_2 = request.POST.get('pswd_2', None)
            try:
                customer = Customer.objects.get(id=id_customer)
                customer.user.set_password(pswd_2)
                customer.save()
                message = '¡Se ha cambiado exitosamente la contraseña del cliente!'
                ctx = {'message': message}
                return HttpResponse(json.dumps(ctx), content_type='application/json')

            except Exception as e:
                message = str(e)
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response

        else:
            response = JsonResponse({"error": "there was an error"})
            response.status_code = 403
            return response


# ====================== Store ======================
class CategoriesListView(LoginRequiredMixin, TemplateView):
    template_name = "panel/admins/store/categories/categories_list.html"

    def get_context_data(self, **kwargs):
        context = super(CategoriesListView, self).get_context_data(**kwargs)
        context['categories'] = ProductCategory.objects.all().order_by('-created_at')
        return context


@login_required
def admins_categories_createview(request):

    form = AdminCategoryCreateForm(request.POST or None, request.FILES)

    if form.is_valid():
            category = form.save()

            if category.parent != None:

                if category.parent.has_children == False:
                    category.parent.has_children = True
                    category.parent.save()
                        
            messages.success(
                request, 'Categoría creada exitosamente'
            )
            return HttpResponseRedirect(reverse('admins_categories_listview'))

    return render(request, 'panel/admins/store/categories/categories_create.html', {'form': form})


@login_required
def admin_category_edit(request, *args, **kwargs):

    id_old_parent = ""
    category = ProductCategory.objects.get(pk=kwargs['pk'])
    old_category = ""

    if category.parent != None:
        id_old_parent = category.parent.id
        old_category = ProductCategory.objects.get(id=id_old_parent)


    form = AdminCategoryEditForm(request.POST or None, request.FILES or None, instance=category)

    if request.method == 'POST':

        if form.is_valid():
            category = form.save()


            if category.parent != None:

                if category.parent.has_children == False:
                    category.parent.has_children = True
                    category.parent.save()
                

            if category.parent != old_category:
                if id_old_parent != "":
                    if old_category.children.all().count() == 0:
                        old_category.has_children = False
                        old_category.save()
                        
            messages.success(
                request, 'Categoría modificada exitosamente'
            )
            return HttpResponseRedirect(reverse('admins_categories_listview'))

    return render(request, 'panel/admins/store/categories/categories_edit.html', {'form': form, 'category':category})


@require_POST
@login_required
def admin_category_archive(request):

    message = ""

    if request.method == 'POST':
        id_category = request.POST.get('id_category', None)
        try:
            category = ProductCategory.objects.get(id=id_category)

            if category.is_active:
                category.is_active = False
                category.save()
                message = '¡Se ha archivado la categoría!'
            else:
                category.is_active = True
                message = '¡Se ha activado la categoría!'
                category.save()
        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


# Products
class ProductsListView(LoginRequiredMixin, TemplateView):
    template_name = "panel/admins/store/products/products_list.html"

    def get_context_data(self, **kwargs):
        context = super(ProductsListView, self).get_context_data(**kwargs)
        products = Product.objects.all().order_by('-created_at')
        context['products'] = products
        context['own_products'] = products.filter(created_by__is_staff=True).count()
        context['thrids_products'] = products.filter(created_by__is_staff=False).count()
        return context


@login_required
def admins_products_createview(request):

    form = AdminProductCreateForm(request.POST or None, request.FILES or None)

    if request.method == 'POST':

        if form.is_valid():
            product = form.save(commit=False)            
            product.created_by = request.user
            product.is_active = True
            product.status = 1
            product.save()

            product_action = ProductAction()
            product_action.created_by = request.user
            product_action.product = product
            product_action.action = "Creación"
            now = datetime.now()
            product_action.details = "El producto fue creado por el usuario '{}' y pasó al estado PUBLICADO. Fecha {}".format(request.user, now.strftime("%d/%m/%Y %I:%M %p"))
            product_action.save()
                        
            messages.success(
                request, 'Producto creado y publicado exitosamente'
            )
            url = reverse('admin_products_detail', kwargs={'pk': product.id})
            return HttpResponseRedirect(url)

    return render(request, 'panel/admins/store/products/products_create.html', {'form': form})


@login_required
def admin_products_detail(request, *args, **kwargs):
    product = Product.objects.get(pk=kwargs['pk'])
    history = ProductAction.objects.filter(product=product).order_by('-created_at')
    return render(request, 'panel/admins/store/products/products_detail.html', {'product': product, 'history': history})


@login_required
def admin_products_edit(request, *args, **kwargs):
    
    product = Product.objects.get(pk=kwargs['pk'])

    form = AdminProductEditForm(request.POST or None, request.FILES or None, instance=product)

    if request.method == 'POST':

        if form.is_valid():
            product = form.save()
            messages.success(
                request, 'Producto editado exitosamente'
            )
            url = reverse('admin_products_detail', kwargs={'pk': product.id})
            return HttpResponseRedirect(url)

    return render(request, 'panel/admins/store/products/products_edit.html', {'form':form, 'product': product})


@require_POST
@login_required
def admin_publish_product(request):

    status = 0

    message = ""

    if request.method == 'POST':
        id_product = request.POST.get('id_product', None)
        try:
            product = Product.objects.get(id=id_product)

            if request.user.is_staff:
                status = product.status
                product.status = 1
                product.save()

                if status == 0:
                    status = "Pendiente"
                elif status == 1:
                     status =  "Publicado"
                elif status == 2:
                    status = "Rechazado"
                elif status == 3:
                    status = "Pausado"

                message = '¡Producto pasó a estar publicado!'
                product_action = ProductAction()
                product_action.created_by = request.user
                product_action.product = product
                product_action.action = "Publicación"
                now = datetime.now()
                product_action.details = "El producto pasó de estar en estado {} a estar publicado. Fecha {}".format(status, now.strftime("%d/%m/%Y %I:%M %p"))
                product_action.save()
            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')

@require_POST
@login_required
def admin_hide_product(request):

    status = 0

    message = ""

    if request.method == 'POST':
        id_product = request.POST.get('id_product', None)
        try:
            product = Product.objects.get(id=id_product)

            if request.user.is_staff:
                status = product.status
                product.status = 0
                product.save()

                if status == 0:
                    status = "Pendiente"
                elif status == 1:
                     status =  "Publicado"
                elif status == 2:
                    status = "Rechazado"
                elif status == 3:
                    status = "Pausado"

                message = '¡Producto pasó a estar oculto!'
                product_action = ProductAction()
                product_action.created_by = request.user
                product_action.product = product
                product_action.action = "Ocultar"
                now = datetime.now()
                product_action.details = "El producto pasó de estar en estado {} a estar OCULTO. Fecha {}".format(status, now.strftime("%d/%m/%Y %I:%M %p"))
                product_action.save()
            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')

@require_POST
@login_required
def admin_reject_product(request):

    status = 0

    message = ""

    if request.method == 'POST':
        id_product = request.POST.get('id_product', None)
        details = request.POST.get('details', None)
        try:
            product = Product.objects.get(id=id_product)

            if request.user.is_staff:
                status = product.status
                product.status = 2
                product.save()

                if status == 0:
                    status = "Pendiente"
                elif status == 1:
                     status =  "Publicado"
                elif status == 2:
                    status = "Rechazado"
                elif status == 3:
                    status = "Pausado"

                message = '¡Producto se ha rechazado exitosamente!'
                product_action = ProductAction()
                product_action.created_by = request.user
                product_action.product = product
                product_action.action = "Rechazo"
                now = datetime.now()
                product_action.details = "El producto pasó de estar en estado {} a estar RECHAZADO. Motivo: {} Fecha {}".format(status, details, now.strftime("%d/%m/%Y %I:%M %p"))
                product_action.save()
            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


@require_POST
@login_required
def admin_add_stock_product(request):

    message = ""

    if request.method == 'POST':
        id_product = request.POST.get('id_product', None)
        qqty_stock = request.POST.get('qqty_stock', None)
        try:
            product = Product.objects.get(id=id_product)

            old_stock = product.available_units

            if request.user.is_staff:
                
                product.available_units += int(qqty_stock)
                product.save()

                message = '¡Nuevas existencias agregadas al producto exitosamente!'
                product_action = ProductAction()
                product_action.created_by = request.user
                product_action.product = product
                product_action.action = "Adición"
                now = datetime.now()
                product_action.details = "El producto pasó de tener {} unidades a {}. Fecha {}".format(old_stock, product.available_units, now.strftime("%d/%m/%Y %I:%M %p"))
                product_action.save()
            else:
                message = "Prohibido"
                response = JsonResponse({"error": message})
                response.status_code = 409
                return response

        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


# Stores
class StoresListView(LoginRequiredMixin, TemplateView):
    template_name = "panel/admins/store/stores/stores_list.html"

    def get_context_data(self, **kwargs):
        context = super(StoresListView, self).get_context_data(**kwargs)
        stores = CompanyProfile.objects.all().order_by('-created_at')
        context['stores'] = stores
        context['own_stores'] = stores.filter(is_official=True).count()
        context['thrids_stores'] = stores.filter(is_official=False).count()
        return context


@login_required
def admin_stores_detail(request, *args, **kwargs):
    store = CompanyProfile.objects.get(pk=kwargs['pk'])
    products = Product.objects.filter(seller=store.customer).order_by('-created_at')
    return render(request, 'panel/admins/store/stores/stores_detail.html', {'store': store, 'products': products})


@login_required
def admin_stores_edit(request, *args, **kwargs):
    
    store = CompanyProfile.objects.get(pk=kwargs['pk'])

    form = AdminStoreEditForm(request.POST or None, request.FILES or None, instance=store)

    if request.method == 'POST':

        if form.is_valid():
            store = form.save()
            messages.success(
                request, 'Tienda editada exitosamente'
            )
            url = reverse('admin_stores_detail', kwargs={'pk': store.id})
            return HttpResponseRedirect(url)

    return render(request, 'panel/admins/store/stores/stores_edit.html', {'form':form, 'store': store})


@login_required
def admins_stores_createview(request):

    form = AdminStoreCreateForm(request.POST or None, request.FILES or None)

    if request.method == 'POST':

        if form.is_valid():
            store = form.save()
                        
            messages.success(
                request, 'Tienda creada exitosamente'
            )
            url = reverse('admin_stores_detail', kwargs={'pk': store.id})
            return HttpResponseRedirect(url)

    return render(request, 'panel/admins/store/stores/stores_create.html', {'form': form})


@require_POST
@login_required
def admin_change_store_state(request):

    message = ""

    if request.method == 'POST':
        id_store = request.POST.get('id_store', None)
        try:
            store = CompanyProfile.objects.get(id=id_store)

            if store.is_active:
                store.is_active = False
                store.save()
                message = '¡Se ha desactivado la tienda exitosamente!'
            else:
                store.is_active = True
                message = '¡Se ha activado la tienda exitosamente!'
                store.save()
        except Exception as e:
            message = str(e)
            response = JsonResponse({"error": message})
            response.status_code = 409
            return response

        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')
