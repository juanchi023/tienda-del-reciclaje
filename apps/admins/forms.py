from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm
from apps.customers.models import *
from apps.store.models import *
from django.db.models import Q


class EmailAuthForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput, required=True)

    def __init__(self, *args, **kwargs):
        self.user_cache = None
        super(EmailAuthForm, self).__init__(*args, **kwargs)

    def clean(self):

        self.cleaned_data = super(EmailAuthForm, self).clean()
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        self.user_cache = authenticate(email=email, password=password)

        if self.user_cache is None:
            self._errors["email"] = self.error_class(['Email o contraseña incorrecta!'])
        elif not self.user_cache.is_active:
            self._errors["email"] = self.error_class(['¡Usuario inactivo o email no confirmado!'])
        elif not self.user_cache.is_staff:
            if not self.user_cache.usercustomer.company.is_active:
                self._errors["email"] = self.error_class(['¡Empresa no activa! Contáctenos para más información...'])

        return self.cleaned_data

    def get_user(self):
        return self.user_cache


class AdminCustomerCreateForm(UserCreationForm):

    email = forms.EmailField()
    phone = forms.CharField()

    def clean_email(self):
        data = self.cleaned_data['email']

        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("Este email ya lo tiene otro usuario")

        return data

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'phone',
        )


class AdminCustomerEditForm(forms.ModelForm):
	
    email = forms.EmailField()
    first_name = forms.CharField()
    last_name = forms.CharField()
    birth_date = forms.DateField(
        widget=forms.DateInput(format='%Y-%m-%d'),
        input_formats=('%Y-%m-%d', )
    )

    class Meta:
        model = Customer
        fields = (
            'first_name',
            'last_name',
            'email',
            'phone',
            'id_type',
            'id_number',
            'profile_img',
            'birth_date',
        )


    def clean_email(self):
        data = self.cleaned_data['email']
        
        users = User.objects.filter(email__iexact=data).exclude(email__iexact=data)

        if users:
            raise forms.ValidationError("Este email ya lo tiene otro usuario")

        return data
	
    def clean_profile_img(self):
        profile_img = self.cleaned_data.get('profile_img', False)
        if profile_img:
            if profile_img.size > 4*1024*1024:
                raise forms.ValidationError("Imagen muy pesada ( > 4mb )")
            return profile_img


class AdminCategoryCreateForm(forms.ModelForm):
    

    class Meta:
        model = ProductCategory
        fields = (
            'name',
            'img',
            'description',
            'commission',
            'parent',
            'featured',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['parent'].queryset = ProductCategory.objects.exclude(~Q(parent=None))


class AdminCategoryEditForm(forms.ModelForm):
    

    class Meta:
        model = ProductCategory
        fields = (
            'name',
            'img',
            'description',
            'commission',
            'parent',
            'featured',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = self.instance
        if instance.pk is not None:
            self.fields['parent'].queryset = ProductCategory.objects.exclude(Q(pk=instance.pk)|~Q(parent=None))


class AdminProductCreateForm(forms.ModelForm):
    

    class Meta:
        model = Product
        fields = (
            'title',
            'category',
            'description',
            'main_img',
            'sku',
            'unit_price',
            'discount_price',
            'quantity_per_unit',
            'available_units',
            'in_bid',
            'bid_price_start_at',
            'bid_date_start',
            'bid_date_end',
            'required_documents',
        )
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].queryset = ProductCategory.objects.exclude(is_active=False)


class AdminProductEditForm(forms.ModelForm):

    bid_date_start = forms.DateField(
        widget=forms.DateInput(format='%Y-%m-%d'),
        input_formats=('%Y-%m-%d', ),
        required=False
    )

    bid_date_end = forms.DateField(
        widget=forms.DateInput(format='%Y-%m-%d'),
        input_formats=('%Y-%m-%d', ),
        required=False
    )
    

    class Meta:
        model = Product
        fields = (
            'title',
            'category',
            'description',
            'main_img',
            'sku',
            'unit_price',
            'discount_price',
            'quantity_per_unit',
            'in_bid',
            'bid_price_start_at',
            'bid_date_start',
            'bid_date_end',
            'required_documents',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['category'].queryset = ProductCategory.objects.exclude(is_active=False)


class AdminStoreCreateForm(forms.ModelForm):
    
    class Meta:
        model = CompanyProfile
        fields = (
            'customer',
            'name',
            'id_type',
            'id_number',
            'address',
            'email',
            'zip_code',
            'phone',
            'website_url',
            'logo',
            'cover_img',
            'is_official',
        )


class AdminStoreEditForm(forms.ModelForm):
    
    class Meta:
        model = CompanyProfile
        fields = (
            'customer',
            'name',
            'id_type',
            'id_number',
            'address',
            'email',
            'zip_code',
            'phone',
            'website_url',
            'logo',
            'cover_img',
            'is_official',
        )