from django.urls import path, include
from apps.admins import views as admins_views

urlpatterns = [
    path('admins/panel/', admins_views.AdminsPanelView.as_view(), name='admins_panel_view'),

    # Customers
    path('panel/admins/gestion-clientes/', admins_views.CustomersListView.as_view(), name='admins_customers_listview'),

    path('panel/admins/gestion-clientes/crear/', admins_views.admin_custormer_create, name='admin_custormer_create'),

    path('panel/admins/gestion-clientes/get-details/', admins_views.admin_get_customer_details, name='admin_get_customer_details'),

    path('panel/admins/gestion-clientes/change-password/', admins_views.admin_change_pswd_customer, name='admin_change_pswd_customer'),

    path('panel/admins/gestion-clientes/<pk>/detalles/', admins_views.CustomersDetailView.as_view(), name='admins_customers_detailview'),

    path('panel/admins/gestion-clientes/<pk>/editar/', admins_views.admin_customer_edit, name='admin_customer_edit'),

    path('panel/admins/gestion-clientes/change-state/', admins_views.admin_change_customer_state, name='admin_change_customer_state'),

    # ============ Store ==================

    # Categories
    path('panel/admins/gestion-tienda/categorias/', admins_views.CategoriesListView.as_view(), name='admins_categories_listview'),

    path('panel/admins/gestion-tienda/categorias/crear/', admins_views.admins_categories_createview, name='admins_categories_createview'),

    path('panel/admins/gestion-tienda/categorias/<pk>/editar/', admins_views.admin_category_edit, name='admin_category_edit'),

    path('panel/admins/gestion-tienda/categorias/cambiar-estado/', admins_views.admin_category_archive, name='admin_category_archive'),

    # Products
    path('panel/admins/gestion-tienda/productos/', admins_views.ProductsListView.as_view(), name='admins_products_listview'),

    path('panel/admins/gestion-tienda/productos/crear/', admins_views.admins_products_createview, name='admins_products_createview'),

    path('panel/admins/gestion-tienda/productos/<pk>/detalles/', admins_views.admin_products_detail, name='admin_products_detail'),

    path('panel/admins/gestion-tienda/productos/<pk>/editar/', admins_views.admin_products_edit, name='admin_products_edit'),

    path('panel/admins/gestion-tienda/productos/publicar/', admins_views.admin_publish_product, name='admin_publish_product'),

    path('panel/admins/gestion-tienda/productos/ocultar/', admins_views.admin_hide_product, name='admin_hide_product'),

   path('panel/admins/gestion-tienda/productos/rechazar/', admins_views.admin_reject_product, name='admin_reject_product'), 

   path('panel/admins/gestion-tienda/productos/agregar-unidades/', admins_views.admin_add_stock_product, name='admin_add_stock_product'),

   # Stores
   path('panel/admins/gestion-tienda/tiendas/', admins_views.StoresListView.as_view(), name='admins_stores_listview'),

   path('panel/admins/gestion-tienda/tiendas/crear/', admins_views.admins_stores_createview, name='admins_stores_createview'),

   path('panel/admins/gestion-tienda/tiendas/<pk>/detalles/', admins_views.admin_stores_detail, name='admin_stores_detail'),

   path('panel/admins/gestion-tienda/tiendas/<pk>/editar/', admins_views.admin_stores_edit, name='admin_stores_edit'),

   path('panel/admins/gestion-tienda/tiendas/cambiar-estado/', admins_views.admin_change_store_state, name='admin_change_store_state'),
]
