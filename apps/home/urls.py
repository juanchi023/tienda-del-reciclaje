from django.urls import path, include
from apps.home import views as home_views

urlpatterns = [
    path('', home_views.home_view, name='home_view'),
    path('nosotros/', home_views.about_view, name='about_view'),

    path('contacto/', home_views.contacts_view, name='contacts_view'),

    path('enviar-mensaje/', home_views.register_message_view, name='register_message_view'),

    path('tienda/', home_views.shop_view, name='shop_view'),

    path('tienda/categorias/', home_views.shop_categories_view, name='shop_categories_view'),

    path('tienda/categorias/<slug:slug>/', home_views.shop_filter_by_category_home, name='shop_filter_by_category_home'),
    path('tienda/producto/<slug:slug>/', home_views.product_details_home, name='product_details_home'),

    path('vende-aqui/', home_views.become_seller_view, name='become_seller_view'),

    path('registro/', home_views.user_register_view, name='user_register_view'),
    path('registro/city-filter/', home_views.city_filter, name='city_filter'),
    path('registro-exitoso/', home_views.register_success_view, name='register_success_view'),
    path('activacion/<slug:uidb64>/<slug:token>/', home_views.user_account_activation, name='user_account_activation'), 

    path('login/', home_views.login_view, name='login_view'),
    path('logout/', home_views.logout_view, name='logout_view'),

    path('email/', home_views.email_welcome, name='email_welcome'),

    path('test-payu/', home_views.test_payu, name='test_payu'),
]
