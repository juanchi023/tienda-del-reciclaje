from django.db import models
import os


class Message(models.Model):

    complete_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    subject = models.CharField(max_length=120)
    message = models.TextField(default="")
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{0}: {1} [{2}]".format(self.complete_name, self.email, self.date)