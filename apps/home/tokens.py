from django.contrib.auth.tokens import PasswordResetTokenGenerator
import six
from apps.customers.models import *

class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
	def _make_hash_value(self, user, timestamp):
		customer = Customer.objects.get(user=user)
		return (
			six.text_type(user.pk) + six.text_type(timestamp) +
			six.text_type(customer.email_confirmed)
			)

account_activation_token = AccountActivationTokenGenerator()