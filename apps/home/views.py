from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic import DetailView
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from django.views.decorators.http import require_POST
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from apps.customers.models import *
from apps.store.models import *
from . models import Message
from django.urls import reverse_lazy
from django.contrib import messages
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMultiAlternatives
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .tokens import account_activation_token
from django.utils.encoding import force_bytes, force_text
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode

from apps.customers.forms import *
from .forms import *

import requests


def home_view(request):
    categories = ProductCategory.objects.all().order_by('name')
    featured_categories = categories.filter(featured=True, is_active=True)[:3]
    products = Product.objects.filter(status=1, category__is_active=True).distinct().order_by('-created_at')[:9]
    return render(request, 'home/index.html', {
        'categories': categories,
        'featured_categories':featured_categories,
        'products': products
    })


def about_view(request):
    return render(request, 'home/about.html')

def contacts_view(request):
    return render(request, 'home/contacts.html')


@require_POST
def register_message_view(request):

    if request.method == 'POST':
        complete_name = request.POST.get('complete_name', None)
        email = request.POST.get('email', None)
        subject = request.POST.get('subject', None)
        phone = request.POST.get('phone', None)
        message = request.POST.get('message', None)
        m = Message()

        m.complete_name = complete_name
        m.email = email
        m.phone = phone
        m.subject = subject
        m.message = message
        m.save()

        # send_email_nuevo_mensaje.delay(name, company, email, subject, phone)

        message = '¡Su solicitud se ha registrado! Nos comunicaremos contigo en breve.'
        ctx = {'message': message}
        return HttpResponse(json.dumps(ctx), content_type='application/json')


def shop_view(request):
    categories = ProductCategory.objects.filter(is_active=True).order_by('name')
    products = Product.objects.filter(status=1, category__is_active=True).distinct().order_by('-created_at')
    
    page = request.GET.get('page')
    paginator = Paginator(products, 15)
    products = paginator.get_page(page)
    
    return render(request, 'home/shop.html', {'categories': categories, 'products':products})


def shop_categories_view(request):
    list_categories = ProductCategory.objects.filter(is_active=True).order_by('name')
    categories = list_categories

    page = request.GET.get('page')
    paginator = Paginator(categories, 8)
    categories = paginator.get_page(page)
    
    return render(request, 'home/shop_categories.html', {'categories': categories, 'list_categories': list_categories})


def shop_filter_by_category_home(request, *args, **kwargs):

    list_categories = ProductCategory.objects.filter(is_active=True).order_by('name')
    category_filtered = ProductCategory.objects.get(slug=kwargs.get('slug'))
    products = Product.objects.filter(Q(category=category_filtered) | Q(category__parent=category_filtered)).order_by('-created_at')

    products.filter(status=1)
    
    paginator = Paginator(products, 15)
    page = request.GET.get('page')
    products = paginator.get_page(page)

    return render(request, 'home/shop_filter_cat.html',
    {
        'list_categories': list_categories,
        'category_filtered':category_filtered,
        'products':products
    })


def product_details_home(request, *args, **kwargs):

    list_categories = ProductCategory.objects.filter(is_active=True).order_by('name')
    product = Product.objects.get(slug=kwargs.get('slug'))
    related_products = Product.objects.filter(category=product.category, status=1).exclude(id=product.id).distinct()[:10]

    return render(request, 'home/product_details.html',
    {
        'list_categories': list_categories,
        'product':product,
        'related_products': related_products
    })


def become_seller_view(request):
    return render(request, 'home/become_seller.html')


def user_register_view(request):
    form = CustomerRegisterForm(request.POST or None)
    countries = Country.objects.all().order_by('-name')
    if request.method == 'POST':

        user_type = request.POST.get('user_type', None)
        id_city = request.POST['id_city']
        phone = request.POST['phone']

        city = City.objects.get(id=id_city)

        print('TIPO: ', user_type)

        if form.is_valid():
            user = form.save()
            user.username = request.POST['email']
            user.is_active = False
            user.save()
            customer = Customer()
            customer.user = user
            customer.city = city
            customer.phone = phone
            if user_type == '2':
                customer.user_type = 'Jurídica'
                customer.is_company = True
                customer.company_name = request.POST['company_name']
                customer.company_id_type = request.POST['company_id_type']
                customer.company_id_number = request.POST['company_id_number']
            else:
                customer.user_type = 'Natural'
            customer.save()
            site = get_current_site(request)
            current_site = site.domain
            send_email_to_customer(customer.id, request.POST['password1'], current_site)
            return HttpResponseRedirect(reverse('register_success_view'))

    return render(request, 'home/register.html', {'form': form, 'countries': countries})


@require_POST
def city_filter(request):

    list_cities = []
    country = ""
    cities = ""

    if request.method == 'POST':
        id_country = request.POST.get('id_country', None)

        id_customer = request.POST.get('id_customer', None)

        country = Country.objects.get(id=id_country)

        cities = City.objects.filter(country=country).order_by('name')

        try:
            customer = Customer.objects.get(id=id_customer)

            if customer:
                for city in cities:
                    
                    if customer.city == city:
                        list_cities.append({
                            'id': city.id,
                            'name': city.name,
                            'selected': True
                        })
                        
                    else:
                        list_cities.append({
                            'id': city.id,
                            'name': city.name,
                            'selected': False
                        })

        except Exception as e:
            for city in cities:
                list_cities.append({
                        'id': city.id,
                        'name': city.name
                    })

        return HttpResponse(json.dumps(list_cities), content_type='application/json')


def send_email_to_customer(id_customer, password, current_site_1):

    customer = Customer.objects.get(id=id_customer)
    user = customer.user

    current_site = current_site_1
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    token = account_activation_token.make_token(user)

    htmly = get_template('home/email_welcome.html')

    d = {
        'full_name': customer.get_full_name(),
        'email': customer.user.email,
        'customer': customer,
        'password': password,
        'confirmation_link': "http://{0}{1}".format(current_site,reverse("user_account_activation", kwargs=dict(uidb64=uid, token=token))),
    }

    email = customer.user.email

    subject, from_email, to = 'Te damos la bienvenida a Tienda del Reciclaje', 'noreply@tiendadelreciclaje.com', email
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def user_account_activation(request, uidb64, token):
	try:		
		uid = force_text(urlsafe_base64_decode(uidb64))
		user = User.objects.get(pk=uid)
		customer = Customer.objects.get(user=user)
	except (TypeError, ValueError, OverflowError, User.DoesNotExist):
		user = None

	if user is not None and account_activation_token.check_token(user, token):
		user.is_active = True
		customer.email_confirmed = True
		user.save()
		customer.save()
		login(request, user, backend='ecommerce.estandar.backends.EmailBackend')
		messages.success(request, '¡Su cuenta fue confirmada y activada exitosamente!')
		url = reverse('customers_panel_view')
		return HttpResponseRedirect(url)
	else:
		messages.error(request, 'Link de activación inválido, contáctenos para resolver su problema...')
		return render(request, 'customers/account_activation_invalid.html')


def register_success_view(request):
    return render(request, 'home/register_success.html')


def login_view(request):    

    if not request.user.is_anonymous:
        if request.user.is_staff:
            return HttpResponseRedirect(reverse('admins_panel_view'))
        elif request.user.customer:
            return HttpResponseRedirect(reverse('customers_panel_view'))
    else:
        form = UserAuthForm(request.POST or None)

        if form.is_valid():
            login(request, form.get_user())
            if request.user.is_staff:
                return HttpResponseRedirect(reverse('admins_panel_view'))
            elif request.user.customer:
                return HttpResponseRedirect(reverse('customers_panel_view'))
            else:
                return HttpResponseRedirect(reverse('login_view'))

    return render(request, 'home/login.html', {'form': form})


@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('home_view'))


def email_welcome(request):
    return render(request, 'home/email_welcome.html')


def test_payu(request):
    headers = {
        'Content-type': 'application/json',
        'Accept': 'application/json'
    }
    URL = "https://api.payulatam.com/payments-api/4.0/service.cgi"
    PARAMS = {
        "test": True,
        "language": "es",
        "command": "GET_PAYMENT_METHODS",
        "merchant": {
            "apiLogin": "2LVicZsmMkY9W3B",
            "apiKey": "iVqZTqCnho2039XwQa4985QS3Z"
        }
    }
    
    r = requests.post(url=URL, data=json.dumps(PARAMS), headers=headers)

    return render(request, 'test.html', {'data': r.text})
