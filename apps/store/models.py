import os
from django.db import models
from apps.customers.models import *
from ecommerce.estandar.models import BaseModel
from django.template.defaultfilters import slugify
import uuid


def upload_category_img(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'categories/{0}/{1}{2}'.format(
        instance.slug,
        filename_base,
        filename_ext.lower()
    )


class ProductCategory(BaseModel):
    name = models.CharField(max_length=100)
    img = models.ImageField(upload_to=upload_category_img, blank=True, null=True)
    banner = models.ImageField(upload_to=upload_category_img, blank=True, null=True)
    description = models.TextField()
    commission = models.FloatField(default=0)
    has_children = models.BooleanField(default=False)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='children', on_delete=models.CASCADE)
    slug = models.SlugField(unique=True, editable=True, null=True, blank=True, max_length=150)
    featured = models.BooleanField(default=False)


    class Meta:
        verbose_name = 'Categoría de Producto'
        verbose_name_plural = 'Categorías de Productos'

    def __str__(self):
        if self.parent:
            return '{} - de categoría {}'.format(self.name, self.parent.name)
        else:
            return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(ProductCategory, self).save(*args, **kwargs)


def upload_product_img(instance, filename):
    print("ENTERED")
    
    filename_base, filename_ext = os.path.splitext(filename)
    
    if instance.created_by != None:
        return 'companies/TiendaDelReciclaje/{0}/products/{1}/{2}{3}'.format(
            instance.created_by.username,
            instance.slug,
            filename_base,
            filename_ext.lower()
        )
        
    elif instance.seller != None:
        return 'companies/{0}/products/{1}/{2}{3}'.format(
            instance.seller.user.username,
            instance.slug,
            filename_base,
            filename_ext.lower()
        )


class Product(BaseModel):

    title = models.CharField(max_length=150)
    category = models.ForeignKey(ProductCategory, related_name="products", on_delete=models.SET_NULL, blank=True, null=True)
    description = models.TextField()
    main_img = models.ImageField(upload_to=upload_product_img, blank=False, null=False)
    sku = models.CharField(max_length=150)
    seller = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True, related_name="products")
    unit_price = models.BigIntegerField(default=0)
    discount_price = models.BigIntegerField(default=0, blank=True, null=True)
    quantity_per_unit = models.IntegerField(default=0)
    available_units = models.IntegerField(default=0)
    slug = models.SlugField(unique=True, editable=True, null=True, blank=True, max_length=150)
    in_bid = models.BooleanField(default=False)
    bid_price_start_at = models.BigIntegerField(default=0, null=True, blank=True)
    bid_date_start = models.DateTimeField(null=True, blank=True)
    bid_date_end = models.DateTimeField(null=True, blank=True)
    required_documents = models.TextField(blank=True, null=True)
    status = models.IntegerField(default=0)

    """
    Product status
    0 - Pending
    1 - Published
    2 - Rejected
    3 - Paused
    4 - Bid Closed
    """

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __str__(self):
        if not self.seller:
            return "{} - Vendedor Tienda oficial - Encargado: {}".format(self.title, self.created_by.username)
        else:
            return "{} - Vendedor: {}".format(self.title, self.seller.store.name)
    
    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)
        super(Product, self).save(*args, **kwargs)
    
    def get_price(self):
        if self.discount_price != 0:
            return self.discount_price
        return self.unit_price


class ProductAction(BaseModel):

    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='history')
    action = models.CharField(max_length=50)
    details = models.TextField()

    class Meta:
        verbose_name = 'Acción sobre Producto'
        verbose_name_plural = 'Acciones sobre Productos'

    def __str__(self):
        return "{} - {} {} producto: {}".format(
            self.created_at.strftime("%d/%m/%Y %I:%M %p"),
            self.created_by.username,
            self.action,
            self.product.title
        )


def upload_offer_file(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'companies/{0}/products/{1}/offer/{2}/{3}{4}'.format(
        instance.seller.user.username,
        instance.slug,
        instance.customer.email,
        filename_base,
        filename_ext.lower()
    )


class OfferToProduct(BaseModel):

    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='bidders')
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    price = models.BigIntegerField(default=0)
    is_winner = models.BooleanField(default=False)
    file = models.FileField(upload_to=upload_offer_file, blank=True, null=True)

    class Meta:
        verbose_name = 'Oferta de compradores a producto en subasta'
        verbose_name_plural = 'Ofertas de compradores a productos en subasta'

    def __str__(self):
        return "{} Oferta ${} al producto: {}".format(
            self.customer.get_full_name(),
            self.price,
            self.product.title
        )


def upload_product_galley(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'companies/{0}/products/{1}/gallery/{2}{3}'.format(
        instance.product.seller.user.username,
        instance.product.slug,
        filename_base,
        filename_ext.lower()
    )

class ProductImage(BaseModel):

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    file = models.ImageField(upload_to=upload_product_galley)

    class Meta:
        verbose_name = 'Imagen de Producto'
        verbose_name_plural = 'Imágenes de Productos'

    def filename(self):
        return os.path.basename(self.file.name)

    def __str__(self):
        return "Foto {} del producto {} - Vendedor: {}".format(self.filename(), self.product.title, self.product.seller)


class ProductReview(BaseModel):

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, blank=True, null=True)
    rating = models.IntegerField(default=1)
    comment = models.TextField()

    class Meta:
        verbose_name = 'Reseña de Producto'
        verbose_name_plural = 'Reseñas de Productos'

    def __str__(self):
        return "Producto {} - Cliente: {} - Calificación {}".format(
            self.product.title,
            self.customer.get_full_name(),
            self.rating
        )


class FavoriteProduct(BaseModel):

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name = 'Producto favorito de cliente'
        verbose_name_plural = 'Productos favoritos de clientes'

    def __str__(self):
        return "Producto {} - Cliente: {} - Agregado {}".format(
            self.product.title,
            self.customer.user.email,
            self.created_at.strftime("%d/%m/%Y %I:%M %p"),
        )


class OrderItem(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.quantity} de {self.product.title}"

    def get_total_product_price(self):
        return self.quantity * self.product.get_price()

    def get_total_discount_product_price(self):
        return self.quantity * self.product.discount_price

    def get_amount_saved(self):
        return self.get_total_product_price() - self.get_total_discount_product_price()

    def get_final_price(self):
        if self.product.discount_price != 0:
            return self.get_total_discount_product_price()
        return self.get_total_product_price()


class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='order')
    ref_code = models.CharField(max_length=20, blank=True, null=True)
    products = models.ManyToManyField(OrderItem)
    start_date = models.DateTimeField(auto_now_add=True)
    ordered_date = models.DateTimeField(blank=True, null=True)
    ordered = models.BooleanField(default=False)
    shipping_address = models.ForeignKey(Direction, related_name='shipping_address', on_delete=models.SET_NULL, blank=True, null=True)
    being_delivered = models.BooleanField(default=False)
    received = models.BooleanField(default=False)
    refund_requested = models.BooleanField(default=False)
    refund_granted = models.BooleanField(default=False)

    '''
    1. Item added to cart
    2. Adding a billing address
    (Failed checkout)
    3. Payment
    (Preprocessing, processing, packaging etc.)
    4. Being delivered
    5. Received
    6. Refunds
    '''

    def __str__(self):
        if self.ordered == False:
            return "Orden abierta del usuario: {}".format(self.customer.user.username)
        else:
            return "Orden cerrada del usuario: {}".format(self.customer.user.username)

    def get_total(self):
        total = 0
        for order_item in self.products.all():
            total += order_item.get_final_price()
        return total
    
    def get_total_reward_points(self):
        total = 0
        for order_item in self.products.all():
            total += order_item.get_final_price() / 1000
        return int(total)
