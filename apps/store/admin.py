from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin


@admin.register(
    ProductCategory,
    Product,
    ProductImage,
    OfferToProduct,
    ProductAction,
    FavoriteProduct,
    OrderItem,
    Order,
)
class StoreAdmin(ImportExportModelAdmin):
    pass