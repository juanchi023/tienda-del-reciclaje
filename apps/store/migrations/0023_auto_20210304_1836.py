# Generated by Django 3.1.5 on 2021-03-04 23:36

import apps.store.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0022_favoriteproduct'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='offertoproduct',
            options={'verbose_name': 'Oferta de compradores a producto en subasta', 'verbose_name_plural': 'Ofertas de compradores a productos en subasta'},
        ),
        migrations.RenameField(
            model_name='offertoproduct',
            old_name='in_winner',
            new_name='is_winner',
        ),
        migrations.AddField(
            model_name='offertoproduct',
            name='file',
            field=models.FileField(blank=True, null=True, upload_to=apps.store.models.upload_offer_file),
        ),
        migrations.AddField(
            model_name='product',
            name='required_documents',
            field=models.TextField(blank=True, null=True),
        ),
    ]
