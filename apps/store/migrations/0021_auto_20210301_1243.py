# Generated by Django 3.1.5 on 2021-03-01 17:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0020_auto_20210301_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productaction',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='history', to='store.product'),
        ),
    ]
