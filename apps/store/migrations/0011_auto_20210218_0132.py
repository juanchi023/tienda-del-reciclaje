# Generated by Django 3.1.5 on 2021-02-18 06:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0010_auto_20210218_0104'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='discount_price',
            field=models.BigIntegerField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='unit_price',
            field=models.BigIntegerField(default=0),
        ),
    ]
