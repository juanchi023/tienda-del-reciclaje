from django import template
from datetime import datetime
import os
from apps.store.models import *
from django.db.models import Q


register = template.Library()


@register.filter
def calculate_discount(product):
    result = ((product.unit_price - product.discount_price) / product.unit_price) * 100
    result = round(result, 1)
    return int(result)


@register.filter
def get_number_products(category):
    num_products = Product.objects.filter(Q(category=category) | Q (category__parent=category)).count() 
    return num_products


@register.filter
def days_until(date):
    delta = datetime.date(date) - datetime.now().date()
    return delta.days
